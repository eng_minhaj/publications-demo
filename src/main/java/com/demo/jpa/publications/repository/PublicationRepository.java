package com.demo.jpa.publications.repository;

import org.springframework.data.repository.CrudRepository;

import com.demo.jpa.publications.bean.Publication;

public interface PublicationRepository extends CrudRepository<Publication, Long> {

}
