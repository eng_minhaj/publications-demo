package com.demo.jpa.publications.repository;

import org.springframework.data.repository.CrudRepository;

import com.demo.jpa.publications.bean.Publisher;

public interface PublisherRepository extends CrudRepository<Publisher, Long> {
	public Publisher findFirstByName(String name);
}
