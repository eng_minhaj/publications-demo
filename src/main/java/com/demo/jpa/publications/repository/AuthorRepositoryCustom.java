package com.demo.jpa.publications.repository;

import java.util.List;

import com.demo.jpa.publications.bean.Author;
import com.demo.jpa.publications.bean.Publisher;

public interface AuthorRepositoryCustom {
	public List<Author> getByAge(int age);
	
	public List<Author> getByPublisher(Publisher publisher);
}
